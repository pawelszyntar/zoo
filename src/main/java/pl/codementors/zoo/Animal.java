package pl.codementors.zoo;

public abstract class Animal {

    private String name;
    private int age;

    public Animal() {
    }

    public Animal(String name, int age) {
        setName(name);
        setAge(age);
    }

    public void returnAnimalName() {
        System.out.println("Name is " + getName() + ".");
    }

    public void returnAnimal() {
        System.out.println("Name is " + getName());
        System.out.println("Age is " + getAge());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();
}
