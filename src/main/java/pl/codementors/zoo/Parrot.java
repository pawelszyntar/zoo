package pl.codementors.zoo;

public class Parrot extends Bird implements Herbivorous {

    public Parrot() {
    }

    public Parrot(String name, int age, String featherColor) {
        super(name, age, featherColor);
    }

    public void screech() {
        returnAnimalName();
        System.out.println("Bird is screeching.");
    }

    @Override
    public void eat() {
        returnAnimalName();
        System.out.println("Bird is eating.");
    }

    @Override
    public void eatPlant() {
        System.out.println("Parrot is eating plant.");
    }

}
