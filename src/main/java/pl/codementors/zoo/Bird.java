package pl.codementors.zoo;

public abstract class Bird extends Animal {

    private String featherColor;

    public Bird() {
    }

    public Bird(String name, int age, String featherColor) {
        super(name, age);
        this.featherColor = featherColor;
    }

    public void returnBird() {
        returnAnimal();
        System.out.println("Color is " + getFeatherColor());
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
