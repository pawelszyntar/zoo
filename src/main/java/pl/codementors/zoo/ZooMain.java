package pl.codementors.zoo;

import java.io.*;
import java.util.Scanner;

public class ZooMain {

    public static void main(String[] args) {

        System.out.println("Hello to the ZOO.");

        Scanner input = new Scanner(System.in);
        System.out.print("Enter number of animals: ");
        int numberOfAnimals = input.nextInt();
        Animal[] animals = new Animal[numberOfAnimals];

        for (int i = 0; i < numberOfAnimals; i++) {
            System.out.println("Kind of animal: \n" +
                    "1 - wolf \n" +
                    "2 - parrot \n" +
                    "3 - iguana");
            int type = input.nextInt();
            System.out.print("Name: ");
            String name = input.next();
            System.out.print("Age: ");
            int age = input.nextInt();
            System.out.print("Color: ");
            String color = input.next();


            if (type == 1) {
                Wolf wolf = new Wolf(name, age, color);
                animals[i] = wolf;
            } else if (type == 2) {
                Parrot parrot = new Parrot(name, age, color);
                animals[i] = parrot;
            } else if (type == 3) {
                Iguana iguana = new Iguana(name, age, color);
                animals[i] = iguana;
            }
        }

//        print(animals);
//        feed(animals);
        saveToFile(animals, "/tmp/tekstowypawelszyntar");
//        howl(animals);
//        hiss(animals);
//        screech(animals);
//        feedWithMeat(animals);
//        feedWithPlant(animals);
        readFromFile("/tmp/tekstowypawelszyntar");
//        printColors(animals);
        saveToBinaryFile(animals, "/tmp/binarnypawelszyntar");
        readFromBinaryFile("/tmp/binarnypawelszyntar");
    }

    static void print(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Wolf) {
                System.out.println("Type is wolf");
                ((Wolf) animal).returnMammal();
            } else if (animal instanceof Parrot) {
                System.out.println("Type is parrot");
                ((Parrot) animal).returnBird();
            } else if (animal instanceof Iguana) {
                System.out.println("Type is iguana");
                ((Iguana) animal).returnLizard();
            }
            System.out.println();
        }
    }

    static void feed(Animal[] animals) {
        for (Animal animal : animals) {
            animal.eat();
        }
    }

    static void saveToFile(Animal[] animals, String fileName) {
        File file = new File(fileName);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (Animal animal : animals) {
                if (animal instanceof Wolf) {
                    bw.write("1\n");
                } else if (animal instanceof Parrot) {
                    bw.write("2\n");
                } else if (animal instanceof Iguana) {
                    bw.write("3\n");
                }
                bw.write(animal.getClass().getSimpleName() + "\n");
                bw.write(animal.getName() + "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    static void howl(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Wolf) {
                ((Wolf) animal).howl();
            }
        }
    }

    static void hiss(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Iguana) {
                ((Iguana) animal).hiss();
            }
        }
    }

    static void screech(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Parrot) {
                ((Parrot) animal).screech();
            }
        }
    }

    static void feedWithMeat(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Carnivorous) {
                ((Carnivorous) animal).eatMeat();
            }
        }
    }

    static void feedWithPlant(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Herbivorous) {
                ((Herbivorous) animal).eatPlant();
            }
        }
    }

    static void readFromFile(String fileName) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            Animal[] newAnimals = new Animal[10];
            int i = 0;
            while (br.ready()) {
                String line = br.readLine();
                int type = Integer.parseInt(line);
                br.readLine();
                String name = br.readLine();
                if (type == 1) {
                    Wolf w = new Wolf();
                    w.setName(name);
                    newAnimals[i] = w;
                } else if (type == 2) {
                    Parrot p = new Parrot();
                    p.setName(name);
                    newAnimals[i] = p;
                } else if (type == 3) {
                    Iguana ig = new Iguana();
                    ig.setName(name);
                    newAnimals[i] = ig;
                }
                i++;
            }
            for (Animal animal : newAnimals) {
                if (animal != null) {
                    System.out.println(animal.getName());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    static void printColors(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Mammal) {
                System.out.println(((Mammal) animal).getFurColor());
            } else if (animal instanceof Lizard) {
                System.out.println(((Lizard) animal).getScaleColor());
            } else if (animal instanceof Bird) {
                System.out.println(((Bird) animal).getFeatherColor());
            }
        }
    }

    static void saveToBinaryFile(Animal[] animals, String fileName) {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            for (Animal element : animals) {
                if (element instanceof Wolf) {
                    oos.writeObject("1");
                } else if (element instanceof Parrot) {
                    oos.writeObject("2");
                } else if (element instanceof Iguana) {
                    oos.writeObject("3");
                }
                oos.writeObject(element.getClass().getSimpleName() + "\n");
                oos.writeObject(element.getName() + "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    static void readFromBinaryFile(String fileName) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {

            Animal[] newAnimals = new Animal[10];

            int i = 0;
            for (; ; ) {
                try {
                    String input = (String) ois.readObject();
                    int type = Integer.parseInt(input);
                    ois.readObject();
                    String name = (String) ois.readObject();
                    if (type == 1) {
                        Wolf w = new Wolf();
                        w.setName(name);
                        newAnimals[i] = w;
                    } else if (type == 2) {
                        Parrot p = new Parrot();
                        p.setName(name);
                        newAnimals[i] = p;
                    } else if (type == 3) {
                        Iguana ig = new Iguana();
                        ig.setName(name);
                        newAnimals[i] = ig;
                    }
                    i++;
                } catch (EOFException exp) {
                    break;
                }

            }
            for (Animal animal : newAnimals) {
                if (animal != null) {
                    System.out.print(animal.getName());
                }
            }

        } catch (ClassNotFoundException | IOException ex) {
            System.out.println(ex);
        }
    }
}
