package pl.codementors.zoo;

public class Wolf extends Mammal implements Carnivorous {

    public Wolf() {
    }

    public Wolf(String name, int age, String furColor) {
        super(name, age, furColor);
    }

    public void howl() {
        returnAnimalName();
        System.out.println("Wolf is howling.");
    }

    @Override
    public void eat() {
        returnAnimalName();
        System.out.println("Wolf is eating.");
    }

    @Override
    public void eatMeat() {
        System.out.println("Wolf is eating meat.");
    }

}
