package pl.codementors.zoo;

public interface Carnivorous {

    void eatMeat();

}
