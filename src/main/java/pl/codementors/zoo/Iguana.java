package pl.codementors.zoo;

public class Iguana extends Lizard implements Herbivorous {

    public Iguana() {
    }

    public Iguana(String name, int age, String scaleColor) {
        super(name, age, scaleColor);
    }

    public void hiss() {
        returnAnimalName();
        System.out.println("Lizard is hissing.");
    }

    @Override
    public void eat() {
        returnAnimalName();
        System.out.println("Lizard is eating.");
    }

    @Override
    public void eatPlant() {
        System.out.println("Lizard is eating plant.");
    }

}
